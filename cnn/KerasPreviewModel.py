# import the necessary packages
from keras.models import Sequential
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.layers.core import Activation, Dropout, Flatten, Dense
from keras import backend as K


class KerasPreviewModel:
    """
    The CIFAR-10 made up model in Keras

    Architecture
        INPUT
        [CONV -> ACT -> BN ]  *2
        POOL -> DROPOUT
        [CONV -> ACT -> BN] * 2
        POOL -> DROPOUT
        FC -> ACT -> BN
        DROPOUT -> FC -> SOFTMAX

    """

    @staticmethod
    def add_conv_act_bn(model, nr_filters, filter_size, stride=(1, 1), chanDim=1, input_shape= None):
        """ Add a layer block to the input model: Conv2D layer, Activation layer, Batch normalisation

        :param model:
        :param nr_filters:
        :param filter_size:
        :param stride:
        :param chanDim:
        :param input_shape:
        :return: the models
        """

        if input_shape is not None:
            model.add(Conv2D(nr_filters, filter_size, strides=stride, padding="same", input_shape=input_shape))
        else:
            model.add(Conv2D(nr_filters, filter_size, strides=stride, padding="same"))

        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=chanDim))

        
    @staticmethod
    def build(width, height, depth, classes):

        model = Sequential()

        inputShape = (height, width, depth)
        chanDim = -1

        # if we are using "channels first", update the input shape
        if K.image_data_format() == "channels_first":
            inputShape = (depth, height, width)
            chanDim = 1

        KerasPreviewModel.add_conv_act_bn(model, nr_filters=32, filter_size=(3, 3), chanDim=chanDim, input_shape=inputShape)  # CONV -> ACT -> BN
        KerasPreviewModel.add_conv_act_bn(model, nr_filters=32, filter_size=(3, 3), chanDim=chanDim)  # CONV -> ACT -> BN

        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(rate=0.25))

        KerasPreviewModel.add_conv_act_bn(model, nr_filters=64, filter_size=(3, 3), chanDim=chanDim)  # CONV -> ACT -> BN
        KerasPreviewModel.add_conv_act_bn(model, nr_filters=64, filter_size=(3, 3), chanDim=chanDim)  # CONV -> ACT -> BN

        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(rate=0.25))

        # Flatten
        model.add(Flatten())
        model.add(Dense(512))
        model.add(Activation("relu"))
        model.add(BatchNormalization())

        model.add(Dropout(rate=0.5))
        model.add(Dense(classes))
        model.add(Activation("softmax"))

        return model

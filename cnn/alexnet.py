"""
AlexNet Keras Implementation

BibTeX Citation:

@inproceedings{krizhevsky2012imagenet,
  title={Imagenet classification with deep convolutional neural networks},
  author={Krizhevsky, Alex and Sutskever, Ilya and Hinton, Geoffrey E},
  booktitle={Advances in neural information processing systems},
  pages={1097--1105},
  year={2012}
}
"""

# Import necessary packages
import argparse

# Import necessary components to build LeNet
from keras import models
from keras import layers as KL
# from keras.layers.core import Dense, Dropout, Activation, Flatten
# from keras.layers.convolutional import Conv2D, MaxPooling2D, ZeroPadding2D
# from keras.layers.normalization import BatchNormalization
from keras import regularizers


def alexnet_model(img_shape=(224, 224, 3), n_classes=10, l2_reg=0.,
                  weights=None):
    """Test
    """

    print('hi"')
    # Initialize model
    alexnet = models.Sequential()
    print('hi2"')

    # Layer 1
    alexnet.add(KL.Conv2D(96, (11, 11), 
                input_shape=img_shape,
                padding='same', 
                kernel_regularizer=regularizers.l2(l2_reg)))
    print('here0')
    alexnet.add(KL.BatchNormalization())
    print('hsere01')
    alexnet.add(KL.Activation('relu'))
    print('here01')
    alexnet.add(KL.MaxPooling2D(pool_size=(2, 2)))
    print('here')
    # # Layer 2
    alexnet.add(KL.Conv2D(256, (5, 5), padding='same'))
    alexnet.add(KL.BatchNormalization())
    alexnet.add(KL.Activation('relu'))
    alexnet.add(KL.MaxPooling2D(pool_size=(2, 2)))

    # Layer 3
    alexnet.add(KL.ZeroPadding2D((1, 1)))
    alexnet.add(KL.Conv2D(512, (3, 3), padding='same'))
    alexnet.add(KL.BatchNormalization())
    alexnet.add(KL.Activation('relu'))
    alexnet.add(KL.MaxPooling2D(pool_size=(2, 2)))

    # Layer 4
    alexnet.add(KL.ZeroPadding2D((1, 1)))
    alexnet.add(KL.Conv2D(1024, (3, 3), padding='same'))
    alexnet.add(KL.BatchNormalization())
    alexnet.add(KL.Activation('relu'))

    # Layer 5
    alexnet.add(KL.ZeroPadding2D((1, 1)))
    alexnet.add(KL.Conv2D(1024, (3, 3), padding='same'))
    alexnet.add(KL.BatchNormalization())
    alexnet.add(KL.Activation('relu'))
    alexnet.add(KL.MaxPooling2D(pool_size=(2, 2)))

    # Layer 6
    alexnet.add(KL.Flatten())
    alexnet.add(KL.Dense(3072))
    alexnet.add(KL.BatchNormalization())
    alexnet.add(KL.Activation('relu'))
    alexnet.add(KL.Dropout(0.5))

    # Layer 7
    alexnet.add(KL.Dense(4096))
    alexnet.add(KL.BatchNormalization())
    alexnet.add(KL.Activation('relu'))
    alexnet.add(KL.Dropout(0.5))

    # Layer 8
    alexnet.add(KL.Dense(n_classes))
    alexnet.add(KL.BatchNormalization())
    alexnet.add(KL.Activation('softmax'))

    if weights is not None:
        alexnet.load_weights(weights)

    print(' done')
    return alexnet


def parse_args():
    """
    Parse command line arguments.

    Parameters:
        None
    Returns:
        parser arguments
    """
    parser = argparse.ArgumentParser(description='AlexNet model')
    optional = parser._action_groups.pop()
    required = parser.add_argument_group('required arguments')
    optional.add_argument('--print_model',
                          dest='print_model',
                          help='Print AlexNet model',
                          action='store_true')
    parser._action_groups.append(optional)
    return parser.parse_args()


if __name__ == "__main__":
    # Command line parameters
    args = parse_args()

    # Create AlexNet model
    model = alexnet_model()

    # Print
    if not args.print_model:
        model.summary()

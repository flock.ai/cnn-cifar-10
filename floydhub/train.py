#!/usr/bin/env python
# coding: utf-8

# # Using Keras on the CIFAR-10 dataset
# 
# We setup a simple first Convolutional Neural Network with Keras

# In[1]:


# import the necessary packages
import argparse
import os

from keras import backend as K
from keras import layers as KL
from keras.models import Sequential
from keras.optimizers import SGD

from helperutils import results, MyDataLoader


def ensure_folder(dir_name):
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument('--debug', dest='debug',
                action='store_true',
                help="Whether to run in debug mode, with little data")
ap.add_argument('--dataset', required=True,
                help="path to input dataset")
ap.add_argument('--continue', dest='cont',
                action='store_true',
                help="Whether to continue an already existing model")
ap.set_defaults(debug=False)
ap.set_defaults(cont=False)

args = vars(ap.parse_args())

# Set input arguments
debug = args["debug"]
verbose = 2
continue_training = args["cont"]
dataset_location = args["dataset"]

output_folder = './output'
models_folder = '{}/models'.format(output_folder)
ensure_folder(models_folder)

# Set data info
height = 32
width = 32
depth = 3

# define a first CNN with Keras
inputShape = (height, width, depth)

# if we are using "channels first", update the input shape
if K.image_data_format() == "channels_first":
    inputShape = (depth, height, width)

# # Prep data for training


# load the training and testing data, scale it into the range [0, 1],
# then reshape the design matrix
print("[INFO] loading CIFAR-10 data...")
(trainX, trainY), (testX, testY), labelNames = MyDataLoader().load_dataset(dataset_location)

print("Training set size X: ", trainX.shape)
print("Training set size Y: ", trainY.shape)
print("Test set size X: ", testX.shape)
print("Test set size Y: ", testY.shape)

# ### Define model

model = Sequential()
# define the first (and only) CONV => RELU layer
model.add(KL.Conv2D(1, (11, 11), padding="valid", input_shape=inputShape))
model.add(KL.Activation("relu"))

# softmax classifier
model.add(KL.Flatten())
model.add(KL.Dense(10))
model.add(KL.Activation("softmax"))

model.summary()

name = 'CNN-ShallowNet-a0.01'

# ## Training the model

# train the model using SGD
print("[INFO] training network...")
optimizer = SGD(0.01)
epochs = 2

model.compile(loss="categorical_crossentropy", optimizer=optimizer,
              metrics=["accuracy"])

H = model.fit(trainX[:100], trainY[:100], validation_data=(testX[:100], testY[:100]),
              epochs=epochs, batch_size=32, verbose=1)

results.save_model2(model, filename=name)  # leave out history

# ## Evaluate

# evaluate the network
print("[INFO] evaluating network...")
results.evaluate(model, testX, testY, label_names=labelNames, filename='{}/{}'.format(output_folder, name))

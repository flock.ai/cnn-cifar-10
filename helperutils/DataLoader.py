import cv2
import numpy as np

from imutils import paths
from sklearn.preprocessing import LabelBinarizer

dataset_folder = './dataset'
train_folder = 'train'
test_folder = 'test'

# initialize the label names for the CIFAR-10 dataset
labelNames = ["airplane", "automobile", "bird", "cat", "deer",
              "dog", "frog", "horse", "ship", "truck"]


def load_images(path, verbose=1000):
    # initialize the list of features and labels
    data = []
    labels = []

    image_paths = paths.list_images(path)
    # loop over the input images
    for (i, imagePath) in enumerate(image_paths):
        image = cv2.imread(imagePath)

        data.append(image)
        # filename expected to be dataset/train/trainXYZ-<classnumber>.png
        label = (imagePath.split('-')[-1]).split('.')[0]  # --> <classnumber>.png --> <classnumber>
        labels.append(int(label))

        # show an update every ‘verbose‘ images
        if verbose > 0 and i > 0 and (i + 1) % verbose == 0:
            print("[INFO] processed {}".format(i + 1))

    return np.array(data), np.array(labels)


class MyDataLoader:

    def __init__(self):
        self.train = (np.array([]), np.array([]))
        self.test = (np.array([]), np.array([]))

    def load_dataset(self, location=dataset_folder, verbose=5000):
        print("Loading training images")
        train_location = '{}/{}'.format(location, train_folder)
        trainX, trainY = load_images(train_location, verbose)

        print("Loading test images")
        test_location = '{}/{}'.format(location, test_folder)
        testX, testY = load_images(test_location, verbose)

        trainX = trainX.astype("float") / 255.0
        testX = testX.astype("float") / 255.0

        # convert the labels from integers to vectors
        lb = LabelBinarizer()
        trainY = lb.fit_transform(trainY)
        testY = lb.transform(testY)

        # initialize the label names for the CIFAR-10 dataset
        label_names = ["airplane", "automobile", "bird", "cat", "deer",
                       "dog", "frog", "horse", "ship", "truck"]

        self.train = (trainX, trainY)
        self.test = (testX, testY)

        return self.train, self.test, label_names

import datetime
import os
import pickle
from threading import Thread

import matplotlib.pyplot as plt
import numpy as np
from keras.models import load_model
from sklearn.metrics import classification_report

_OUTPUT_FOLDER = './output'
_MODEL_FOLDER = _OUTPUT_FOLDER + '/models'


def ensure_folder(dir_name):
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)


class Results:
    def __init__(self, experiment_name, output_folder=None, model_folder=None):
        self.name = experiment_name
        self.unique_name = None

        # Conditionally overwrite model/output folder
        self.output_folder = _OUTPUT_FOLDER if output_folder is None else output_folder
        self.model_folder = _MODEL_FOLDER if model_folder is None else model_folder

        ensure_folder(self.output_folder)
        ensure_folder(self.model_folder)

    def get_unique_name(self):
        if self.unique_name is None:
            # Make name unique to save output
            date = datetime.datetime.now().isoformat(timespec="minutes")
            self.unique_name = self.name + '-' + date

        return self.unique_name

    def evaluate(self, model, testX, testY, label_names, verbose=1):
        name = self.get_unique_name()

        predictions = model.predict(testX, batch_size=32)
        report = classification_report(testY.argmax(axis=1),
                                       predictions.argmax(axis=1),
                                       target_names=label_names)

        if verbose is not 0:
            print('Result for {}'.format(name))
            print(report)

        self._save_report(model, report)

    def _save_report(self, model, report):
        results_filename = '{}/{}-results.txt'.format(self.output_folder, self.unique_name)
        with open(results_filename, "w") as file:
            model.summary(print_fn=lambda x: file.write(x + '\n'))
            file.write(report)

    def get_performance_plot(self, train_history, epochs=None):
        if epochs is None:
            epochs = len(train_history.history["loss"])

        print("[INFO] Plotting {} epochs".format(epochs))

        # plot the training loss and accuracy
        plt.style.use("ggplot")
        plt.figure()
        plt.plot(np.arange(0, epochs), train_history.history["loss"][:epochs], label="train_loss")
        plt.plot(np.arange(0, epochs), train_history.history["val_loss"][:epochs], label="val_loss")
        plt.plot(np.arange(0, epochs), train_history.history["acc"][:epochs], label="train_acc")
        plt.plot(np.arange(0, epochs), train_history.history["val_acc"][:epochs], label="val_acc")
        plt.title("Training Loss and Accuracy - {}".format(self.unique_name))
        plt.xlabel("Epoch #")
        plt.ylabel("Loss/Accuracy")
        plt.legend()

        plt.savefig('{}/{}-performance.png'.format(self.output_folder, self.unique_name))
        return plt

    def save_model(self, model, history=None):
        print("[INFO] serializing network...")
        model.save("{}/{}-model.keras".format(self.model_folder, self.unique_name))

        def write():
            with open('{}/{}-trainHistory.dict'.format(self.model_folder, self.unique_name), 'wb') as file:
                pickle.dump(history, file)
                file.close()

        if history is not None:
            print("[INFO] Saving training history...")
            t = Thread(target=write)
            t.start()


if __name__ == "__main__":
    loaded_model = load_model("./results/Convolutional Neural Network (MiniVGGNet)-model.keras")
    with open("./results/Convolutional Neural Network (MiniVGGNet)-trainHistory.dict", "rb") as file_pi:
        H = pickle.load(file_pi)

    print(H)
    print(H.history)

    results = Results('tets')
    results.get_performance_plot(H)

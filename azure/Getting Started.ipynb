{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Running jobs on Azure with the Azure Machine Learning SDK for Python\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using Microsoft's Azure Cloud platform (or any other cloud platform) enables a Deep Learner to train models quicker and with more processing power as compared to training model locally.\n",
    "\n",
    "An ideal scenario would be as follows:\n",
    "  \n",
    "![image](https://raw.githubusercontent.com/MicrosoftDocs/azure-docs/master/articles/machine-learning/service/media/overview-what-is-azure-ml/aml.png)\n",
    "\n",
    "1. Prepare the data as you wish (either local databases, spreadsheets, zip-files, usb sticks, you name it)\n",
    "    * You need to make sure this data becomes available in the Azure cloud in order to use it there\n",
    "1. Develop your models, get basic insights in your dataset and train small models to get a baseline on your favorite system.\n",
    "    * Develop locally on you PC/Laptop\n",
    "    * Use a corporate server with some computational power\n",
    "    * Use a VM in the cloud, such as the [DVSM from Azure](https://azure.microsoft.com/en-us/services/virtual-machines/data-science-virtual-machines/)\n",
    "1. Reach the point where your models take to long to train (>10 mins? >30mins? >2hours). Put the Azure Machine Learning workbench to good use.\n",
    "    * Connect to a workspace in the cloud to which you can push your training jobs \n",
    "    * [Use container instances instead of managing servers](https://azure.microsoft.com/en-us/services/container-instances/). Automatic up and downscaling of resources, depending on your realtime computational load request.\n",
    "1. Analyze and compare results of your experiments online\n",
    "1. More... (pick and deploy?)\n",
    "\n",
    "\n",
    "We're following the approach as layed out here:\n",
    "\n",
    "* [Github Azure Machnine Learning Notebooks](https://github.com/Azure/MachineLearningNotebooks)\n",
    "* [Image classification tutorial with AML](https://docs.microsoft.com/en-us/azure/machine-learning/service/tutorial-train-models-with-aml)\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "# Contents\n",
    "\n",
    "* [Dependencies](#Dependencies)\n",
    "* [Installation and preparation](#Setup-configuration)\n",
    "* [Training on azure](#Training-your-model-on-Azure)\n",
    "* [Tracking the logs of your run](#Tail-on-the-logs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dependencies\n",
    "## Make sure all dependencies are there \n",
    "We assume all necessary packages are already there, other then the specific Azure ones\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip3 install azureml-sdk azureml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Setup configuration\n",
    "\n",
    "Use the `configuration` notebook, to setup your environment to train on (IMPORTANT)\n",
    "[Link to configuration file](./configuration.ipynb)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduce a resource to compute on "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "import azureml.core\n",
    "from azureml.core import Workspace\n",
    "\n",
    "# check core SDK version number\n",
    "print(\"Azure ML SDK Version: \", azureml.core.VERSION)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Connect to a workspace"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load workspace configuration from the config.json file in the current folder.\n",
    "ws = Workspace.from_config()\n",
    "print(ws.name, ws.location, ws.resource_group, ws.location, sep = '\\t')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create an experiment"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "experiment_name = 'CIFAR-10-CNN'\n",
    "\n",
    "from azureml.core import Experiment\n",
    "exp = Experiment(workspace=ws, name=experiment_name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduce our compute cluster we can/will use"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from azureml.core.compute import AmlCompute\n",
    "from azureml.core.compute import ComputeTarget\n",
    "import os\n",
    "\n",
    "# choose a name for your cluster\n",
    "compute_name = os.environ.get(\"AML_COMPUTE_CLUSTER_NAME\", \"cpucluster-1\")\n",
    "compute_min_nodes = os.environ.get(\"AML_COMPUTE_CLUSTER_MIN_NODES\", 0)\n",
    "compute_max_nodes = os.environ.get(\"AML_COMPUTE_CLUSTER_MAX_NODES\", 4)\n",
    "\n",
    "# This example uses CPU VM. For using GPU VM, set SKU to STANDARD_NC6\n",
    "# vm_size = os.environ.get(\"AML_COMPUTE_CLUSTER_SKU\", \"STANDARD_D2_V2\")\n",
    "vm_size = os.environ.get(\"AML_COMPUTE_CLUSTER_SKU\", \"STANDARD_DS3_V2\")\n",
    "\n",
    "\n",
    "if compute_name in ws.compute_targets:\n",
    "    compute_target = ws.compute_targets[compute_name]\n",
    "    if compute_target and type(compute_target) is AmlCompute:\n",
    "        print('found compute target. just use it. ' + compute_name)\n",
    "else:\n",
    "    print('creating a new compute target...')\n",
    "    provisioning_config = AmlCompute.provisioning_configuration(vm_size = vm_size,\n",
    "                                                                min_nodes = compute_min_nodes, \n",
    "                                                                max_nodes = compute_max_nodes)\n",
    "\n",
    "    # create the cluster\n",
    "    compute_target = ComputeTarget.create(ws, compute_name, provisioning_config)\n",
    "\n",
    "    # can poll for a minimum number of nodes and for a specific timeout. \n",
    "    # if no min node count is provided it will use the scale settings for the cluster\n",
    "    compute_target.wait_for_completion(show_output=True, min_node_count=None, timeout_in_minutes=20)\n",
    "\n",
    "     # For a more detailed view of current AmlCompute status, use get_status()\n",
    "    print(compute_target.get_status().serialize())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training your model on Azure\n",
    "\n",
    "## Now we can start with our own fun\n",
    "\n",
    "Let's quickly review our dataset (again)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from  helperutils import MyDataLoader\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from keras.utils import to_categorical\n",
    "print(\"[INFO] loading CIFAR-10 data...\")\n",
    "(trainX, trainY), (testX, testY), labelNames = MyDataLoader().load_dataset(location='../dataset')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_indices = np.random.randint(0, len(trainX), size=5)\n",
    "\n",
    "plt.figure(figsize=(10,100))\n",
    "for (index, i) in enumerate(image_indices):\n",
    "    plt.subplot(1, len(image_indices), index+1)\n",
    "    plt.axis('off')\n",
    "    plt.imshow(trainX[i])\n",
    "    label = np.argmax(trainY[i])\n",
    "    plt.title('#{}: {}'.format(i,labelNames[label]))\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Upload data to cloud"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "datastore = ws.get_default_datastore()\n",
    "print(datastore.datastore_type, datastore.account_name, datastore.container_name)\n",
    "\n",
    "# datastore.upload(src_dir='../dataset', target_path='cifar10', overwrite=True, show_progress=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The actual script that we use for training"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import shutil\n",
    "\n",
    "script_folder  = os.path.join(os.getcwd(), \"tmp\")\n",
    "os.makedirs(script_folder, exist_ok=True)\n",
    "\n",
    "# shutil.copy2('../results.py', script_folder)\n",
    "# shutil.copy2('../DataLoader.py', script_folder)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cp -rv ../helperutils './tmp/'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "sys.argv =['train.py','--debug','--debug', '--dataset','../dataset']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile $script_folder/train.py\n",
    "\n",
    "import os\n",
    "import time\n",
    "import datetime\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "from keras import backend as K\n",
    "from keras import initializers\n",
    "from keras import layers as KL\n",
    "from keras import models\n",
    "from keras import optimizers\n",
    "from keras import regularizers\n",
    "from keras.preprocessing.image import ImageDataGenerator\n",
    "from keras.callbacks import ModelCheckpoint, LambdaCallback\n",
    "\n",
    "from helperutils import results, MyDataLoader\n",
    "\n",
    "\n",
    "from azureml.core import Run\n",
    "import argparse\n",
    "\n",
    "def ensure_folder(dir_name):\n",
    "    if not os.path.exists(dir_name):\n",
    "        os.makedirs(dir_name)\n",
    "\n",
    "\n",
    "# construct the argument parse and parse the arguments\n",
    "ap = argparse.ArgumentParser()\n",
    "ap.add_argument('--debug', dest='debug', \n",
    "                action='store_true', \n",
    "                help=\"Whether to run in debug mode, with little data\")\n",
    "ap.add_argument('--dataset', required=True,\n",
    "                help=\"path to input dataset\")\n",
    "ap.add_argument('--continue', dest='cont',\n",
    "                action='store_true',\n",
    "                help=\"Whether to continue an already existing model\")\n",
    "ap.set_defaults(debug=False)\n",
    "ap.set_defaults(cont=False)\n",
    "\n",
    "\n",
    "args = vars(ap.parse_args())\n",
    "\n",
    "        \n",
    "# Set input arguments\n",
    "debug = args[\"debug\"]\n",
    "verbose = 2\n",
    "continue_training = args[\"cont\"]\n",
    "dataset_location = args[\"dataset\"]\n",
    "\n",
    "# image details\n",
    "height = 32\n",
    "width = 32\n",
    "depth = 3\n",
    "\n",
    "inputShape = (height, width, depth)\n",
    "# if we are using \"channels first\", update the input shape\n",
    "if K.image_data_format() == \"channels_first\":\n",
    "    inputShape = (depth, height, width)\n",
    "\n",
    "    \n",
    "        \n",
    "output_folder = './output'\n",
    "model_folder = '{}/models'.format(output_folder)\n",
    "ensure_folder(model_folder)\n",
    "\n",
    "# Set nr of classes we need to classify\n",
    "classes = 10\n",
    "\n",
    "\n",
    "print(\"[INFO] loading CIFAR-10 data...\")\n",
    "(trainX, trainY), (testX, testY), labelNames = MyDataLoader().load_dataset(location=dataset_location)\n",
    "\n",
    "# Debug\n",
    "if debug:\n",
    "    print(\"[DEBUG] Only using 100 images\")\n",
    "    trainX = trainX[:100]\n",
    "    trainY = trainY[:100]\n",
    "    testX = testX[:100]\n",
    "    testY = testY[:100]\n",
    "\n",
    "print(\"Training set size X: \", trainX.shape)\n",
    "print(\"Training set size Y: \", trainY.shape)\n",
    "print(\"Test set size X: \", testX.shape)\n",
    "print(\"Test set size Y: \", testY.shape)\n",
    "\n",
    "\n",
    "# Example model\n",
    "model = models.Sequential()\n",
    "model.add(KL.Conv2D(96, (5, 5),\n",
    "                    input_shape=(32, 32, 3),\n",
    "                    kernel_initializer='glorot_normal',\n",
    "                    bias_initializer=initializers.Constant(0.1),\n",
    "                    padding='same',\n",
    "                    activation='relu'))\n",
    "\n",
    "model.add(KL.MaxPooling2D((3, 3), padding='valid'))\n",
    "model.add(KL.BatchNormalization())\n",
    "model.add(KL.Dropout(rate=0.1))\n",
    "\n",
    "model.add(KL.Flatten())\n",
    "model.add(KL.Dense(64, \n",
    "                   kernel_initializer='glorot_normal',\n",
    "                   bias_initializer=initializers.Constant(0.1), \n",
    "                   activation='relu'))\n",
    "\n",
    "model.add(KL.Dense(10, \n",
    "                   kernel_initializer='glorot_normal',\n",
    "                   bias_initializer=initializers.Constant(0.1), \n",
    "                   activation='softmax'))\n",
    "\n",
    "model.summary()\n",
    "\n",
    "## Training the model\n",
    "\n",
    "# Setting hyperparameters\n",
    "epochs = 20\n",
    "learning_rate = 0.01\n",
    "decay = 0\n",
    "momentum = 1\n",
    "batch_size= 128\n",
    "\n",
    "# optimizer\n",
    "optimizer = optimizers.SGD(learning_rate)\n",
    "\n",
    "name = 'Template' \n",
    "result = results.Results(name)\n",
    "\n",
    "print(\"[INFO] Checking if there's a set of weights that we can use\")\n",
    "\n",
    "filepath = '{}/{}-best.hdf5'.format(model_folder, name) \n",
    "# If exists a best model, load its weights!\n",
    "if continue_training is True and os.path.isfile(filepath):\n",
    "    print (\"Resumed model's weights from {}\".format(filepath))\n",
    "    # load weights\n",
    "    model.load_weights(filepath)\n",
    "else:\n",
    "    print(\"Nope, starting from scratch\")\n",
    "\n",
    "# Augment training data\n",
    "data_generator = ImageDataGenerator(\n",
    "    featurewise_center=True,\n",
    "    featurewise_std_normalization=True,\n",
    "    rotation_range=20,\n",
    "    width_shift_range=0.2,\n",
    "    height_shift_range=0.2,\n",
    "    horizontal_flip=True)\n",
    "\n",
    "data_generator.fit(trainX)\n",
    "\n",
    "#######\n",
    "\n",
    "\n",
    "# Connecting run to experiment\n",
    "\n",
    "# get hold of the current run\n",
    "run = Run.get_context()\n",
    "\n",
    "\n",
    "tic = time.time()\n",
    "print(\"[INFO] training network...\")\n",
    "\n",
    "# train the model using SGD\n",
    "model.compile(loss=\"categorical_crossentropy\", optimizer=optimizer,\n",
    "                metrics=[\"accuracy\"])\n",
    "\n",
    "\n",
    "checkpoint = ModelCheckpoint(filepath,\n",
    "                            monitor='val_acc',\n",
    "                            verbose=1,\n",
    "                            save_best_only=True,\n",
    "                            mode='max')\n",
    "\n",
    "run_log_callback = LambdaCallback(\n",
    "    on_epoch_end=lambda epoch, logs: run.log('val_acc', logs[\"val_acc\"]))\n",
    "\n",
    "# fits the model on batches with real-time data augmentation:\n",
    "training_data = data_generator.flow(trainX, trainY, batch_size=batch_size)\n",
    "H = model.fit_generator(training_data,\n",
    "                        validation_data=(testX, testY),\n",
    "                        steps_per_epoch=len(trainX) / batch_size,\n",
    "                        epochs=epochs,\n",
    "                        verbose=verbose,\n",
    "                        callbacks=[checkpoint,run_log_callback])\n",
    "\n",
    "# The datagen generates batches of 32 training images / labels per step\n",
    "# steps per epoch is used to determine when an epoch is done (number of step-batches that need to be \n",
    "# generated before an epoch is complete)\n",
    "# https://keras.io/models/model/#fit_generator\n",
    "\n",
    "print('Time taken to train {} epochs: {}'.format(epochs,time.time() - tic))\n",
    "\n",
    "\n",
    "\n",
    "plot = result.get_performance_plot(H)\n",
    "run.log_image('{}-train-history'.format(name), plot=plot)\n",
    "\n",
    "\n",
    "# evaluate the network\n",
    "print(\"[INFO] evaluating network...\")\n",
    "result.evaluate(model, testX,testY, label_names= labelNames)\n",
    "\n",
    "print(\"[INFO] - End reached\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create an estimator\n",
    "An estimator is an encapsulator for your training code and parameters\n",
    "\n",
    "The code below results in the following:\n",
    "\n",
    "\n",
    "- An isolated environment is created on which the training code can be executed. This is a Docker image, which is based on the anaconda3 docker image (because it contains most of the packages we need)\n",
    "- The docker image is put on exactly one node to do our calculations (no distributed training), and we'll use resources from the specified 'compute_target'.\n",
    "- An additional set of pip packages is installed (defined by the `pip_packages` property\n",
    "- The script `train.py` is executed with two parameters: `dataset` and `debug`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from azureml.train.estimator import Estimator\n",
    "\n",
    "script_params = {\n",
    "    '--dataset': datastore.path('cifar10').as_mount(),\n",
    "    '--debug':'--debug'\n",
    "}\n",
    "\n",
    "est = Estimator(source_directory=script_folder,\n",
    "                script_params=script_params,\n",
    "                compute_target=compute_target,\n",
    "                node_count=1,\n",
    "                entry_script='train.py',\n",
    "                custom_docker_image='continuumio/anaconda3',\n",
    "                pip_packages=['azureml',\n",
    "                              'azureml-sdk',\n",
    "                              'tensorflow',\n",
    "                              'imutils',\n",
    "                              'keras',\n",
    "                              'sklearn',\n",
    "                              'numpy',\n",
    "                              'opencv-contrib-python',\n",
    "                              'matplotlib'\n",
    "                             ])\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`exp.submit()` the experiment will push the run to the experiment we created / attached to earlier "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run = exp.submit(config=est)\n",
    "print(run.get_portal_url())\n",
    "run"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tail on the logs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run.wait_for_completion(show_output=True) # specify True for a verbose log"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

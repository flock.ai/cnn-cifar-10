# CNN-Cifar-10

Convolutional Neural networks, optimised for the Cifar-10 dataset


## Rationale
This repository shows a couple of Neural Network architectures, and setups for them. The goal is to get the authors to familiarize themselves with the deep learnin libraries available on the web, as well as the online (cloud) environments that allow for training.


## Models used (in order of build / complexity)
All models are build with Keras

* 'Standard' FeedForward neural network
* [Shallownet (Conv -> Activation -> Dense(10) -> Activation(Softmax)](./Shallownet.ipynb)

* VGG16 (from Keras)
  * Using a built in model in Keras
* DeepWideNet (Wide Neural Net with 4 starting branches of [Conv->MaxPool->BN] x2)
  * This shows merging /concatenating Convolutions Layers together
* Mini Alexnet
  * To see how a popular/common architecture would work
* Mini Inception
  * To see another populer/common architecture.

## Dataset
The CIFAR10 dataset is a very common 'beginners' dataset for image classification. It consists of images of 32x32 (in RGB) of 10 different classes (such as cat, airplane and frog). [Read more](https://www.cs.toronto.edu/~kriz/cifar.html)

The CIFAR10 dataset is available through the KerasAPI in `keras.datasets.cifar10`. To show handling of a dataset you own yourself a handler has been build: `DataLoader.py`. With the `Dataset Generation`-notebook, you can store the custom CIFAR10 dataset in the `dataset` folder. All of the models implemented work well with both the built in model and the custom one. 


## Run job on floydhub
Is doesn't hurt to read the quick-start and tutorial to get familiar with Floydhub:
https://docs.floydhub.com/getstarted/get_started/

1. Make sure floydhub is installed `pip install floyd-cli`, and you are logged in `floyd login`
1. There is a dataset mounted mounted on Floydhub with the CIFAR-10 Dataset ([check here](https://www.floydhub.com/zzave/datasets/cifar-10-keras)]
1. Run a job to make sure everything is working. There's a script `debug.py` that performs most of the actions you'd want during training:
  1. Grab the (preprocessed) dataset.
  1. Setup you model
  1. Define you loss-method and optimizers
  1. Train your model (shortly)
  1. Write your results to disk for future purposes


To run `debug.py`, use the following command: 
```
floyd run --cpu --data zzave/datasets/cifar-10-keras/3:dataset 'bash setup.sh && python debug.py'`
```

If everything runs smooth, you can either run other python scripts, or start writing your own. 
Floydhub expects you to run a Python script instead of a Jupyter notebook (available untill September 2019). 
